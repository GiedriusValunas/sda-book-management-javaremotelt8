package lt.sda.academy.presentation;

import lt.sda.academy.util.ScannerUtil;

public class ApplicationGreeting {

    /**
     * Čia laikysime visą kodą kuris atsakingas už spausdinimą į konsolę arba prašyti paimti informaciją: pvz
     */

    public static int selectMainMenuWindow() {
        System.out.println("Pasirinkite ką norite daryti: ");
        System.out.println("1. Ar daryti operacijas su autoriais?");
        System.out.println("2. Ar daryti operacijas su knygomis?");
        System.out.println("3. Ar daryti operacijas su atsiliepimais?");
        System.out.println("4. Baigti darbą.");
        return ScannerUtil.SCANNER.nextInt();
    }

}
