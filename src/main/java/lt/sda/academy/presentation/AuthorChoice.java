package lt.sda.academy.presentation;

import lt.sda.academy.data.AuthorService;
import lt.sda.academy.printers.AuthorPrinter;
import lt.sda.academy.util.ScannerUtil;

public class AuthorChoice {
    public static void selectAuthorsChoices(AuthorService authorService) {
        int choice = -1;
        while (choice != 0) {
            AuthorPrinter.printAuthorMenuChoices();
            choice = ScannerUtil.SCANNER.nextInt();
            switch (choice) {
                case 1:
                    authorService.getAuthors();
                    break;
                case 2:
                    authorService.findAuthorByKeyword();
                    break;
                case 3:
                    authorService.createAuthor();
                    break;
                case 4:
                    authorService.updateAuthorInfo();
                    break;
                case 5:
                    authorService.deleteAuthor();
                    break;
            }
        }
    }
}
