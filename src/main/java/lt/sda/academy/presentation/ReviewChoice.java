package lt.sda.academy.presentation;

import lt.sda.academy.data.ReviewService;
import lt.sda.academy.printers.ReviewPrinter;
import lt.sda.academy.util.ScannerUtil;

public class ReviewChoice {
    public static void selectReviewChoice(ReviewService reviewService) {
        int choice = -1;
        while (choice != 0) {
            ReviewPrinter.printReviewMenuChoices();
            choice = ScannerUtil.SCANNER.nextInt();
            switch (choice) {
                case 1:
                    reviewService.showReviews();
                    break;
                case 2:
                    reviewService.showBookReviews();
                    break;
                case 3:
                    reviewService.createReview();
                    break;
            }
        }
    }
}
