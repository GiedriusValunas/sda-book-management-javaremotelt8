package lt.sda.academy.presentation;

import lt.sda.academy.data.BookService;

public class BookChoice {

    public static void performOperation(int selection, BookService bookService) {

        switch (selection) {
            case 1:
                bookService.findAllBooks();
                break;
            case 2:
                bookService.findByNameOrByAuthor();
                break;
            case 3:
                bookService.createBook();
                break;
            case 4:
                bookService.updateBook();
                break;
            case 5:
                bookService.deleteBook();
                break;
        }

    }

}
