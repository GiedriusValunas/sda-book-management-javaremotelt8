package lt.sda.academy.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lt.sda.academy.model.shared.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "authors", uniqueConstraints = @UniqueConstraint(columnNames = {"firstname", "lastname"}))
public class Author extends BaseEntity {
    private String firstname;
    private String lastname;

    @Override
    public String toString() {
        return getId() + ", " + firstname + ", " + lastname;
    }
}
