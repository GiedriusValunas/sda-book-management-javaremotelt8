package lt.sda.academy.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lt.sda.academy.model.shared.BaseEntity;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@Entity
@NoArgsConstructor
public class Book extends BaseEntity {

    private String name;
    private String description;
    @ManyToOne(cascade = CascadeType.REMOVE)
    @JoinColumn(foreignKey = @ForeignKey(name = "fk_book_author"), nullable = false)
    private Author author;

    @Override
    public String toString() {
        return "Knyga: " +
                "id=" + getId() +
                ", pavadinimas='" + name + '\'' +
                ", aprašymas='" + description + '\'' +
                ", autorius=" + author;
    }
}
