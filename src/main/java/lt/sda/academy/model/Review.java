package lt.sda.academy.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lt.sda.academy.model.shared.BaseEntity;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;


@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "reviews")
public class Review extends BaseEntity {
    @ManyToOne(cascade = CascadeType.REMOVE)
    @JoinColumn(foreignKey = @ForeignKey(name = "fk_review_book"), nullable = false)
    private Book book;

    @Length(max = 10)
    private int rating;

    private String comment;

    @Override
    public String toString() {
        return getId() + ", Knyga: \""
                + getBook().getName() + "\", "
                + getRating() + ", komentaras: \""
                + getComment() + "\".";
    }
}
