package lt.sda.academy.model.shared;

import lombok.Getter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@MappedSuperclass
@Getter
public class BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @CreationTimestamp
    @Column(nullable = false, columnDefinition = "TIMESTAMP")
    LocalDateTime createDate;

    @UpdateTimestamp
    @Column(nullable = false, columnDefinition = "TIMESTAMP")
    LocalDateTime updateDate;

    public void setId(Long id){
        this.id = id;
    }
}
