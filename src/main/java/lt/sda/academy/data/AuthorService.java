package lt.sda.academy.data;

import lt.sda.academy.data.repositories.AuthorRepository;
import lt.sda.academy.model.Author;
import lt.sda.academy.printers.AuthorPrinter;

import java.util.List;

public class AuthorService {
    private final AuthorRepository REPOSITORY;

    public AuthorService(AuthorRepository repository) {
        this.REPOSITORY = repository;
    }

    public void getAuthors() {
        int limit = 10, offset = 0;
        List<Author> authors;
        do {
            authors = REPOSITORY.findAll(limit, offset);
            AuthorPrinter.printAuthors(authors);
            offset += limit;
            if (authors.size() == limit && !AuthorPrinter.continuePrintAuthors())
                break;
        } while (authors.size() == limit);
        if (!authors.isEmpty()) {
            AuthorPrinter.printAuthorListEnd();
        } else {
            AuthorPrinter.printAuthorListIsEmpty();
        }
    }

    public void findAuthorByKeyword() {
        while (true) {
            String keyword = AuthorPrinter.askAuthorKeyword();
            if (keyword.equals("0"))
                break;
            List<Author> authors = REPOSITORY.findByKeyword(keyword);
            if (authors.isEmpty()) {
                AuthorPrinter.printAuthorsNotFound(keyword);
            } else {
                AuthorPrinter.printAuthors(authors);
            }
        }
    }

    public void createAuthor() {
        Author author = AuthorPrinter.askAuthorInfo(new Author());
        boolean created = author != null && REPOSITORY.save(author);
        AuthorPrinter.printAuthorCreationMessage(created ? author : null);
    }

    public void updateAuthorInfo() {
        long authorId = AuthorPrinter.askAuthorID();
        Author author = REPOSITORY.find(authorId).orElse(null);
        if (author == null) {
            AuthorPrinter.printAuthorByIDNotFound(authorId);
        } else {
            AuthorPrinter.askAuthorInfo(author);
            boolean updated = REPOSITORY.save(author);
            AuthorPrinter.printAuthorUpdateMessage(updated ? author : null);
        }
    }

    public void deleteAuthor() {
        long authorId = AuthorPrinter.askAuthorID();
        Author author = REPOSITORY.find(authorId).orElse(null);
        if (author == null) {
            AuthorPrinter.printAuthorByIDNotFound(authorId);
        } else {
            REPOSITORY.delete(author);
            AuthorPrinter.printAuthorDeleteMessage(author);
        }
    }

    public Author findAuthorById(Long id) {
        return REPOSITORY.findAuthorById(id);
    }

    public void addAuthor(Author author) {
        REPOSITORY.save(author);
    }
}
