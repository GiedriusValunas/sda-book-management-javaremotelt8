package lt.sda.academy.data;

import lt.sda.academy.data.repositories.BookRepository;
import lt.sda.academy.model.Author;
import lt.sda.academy.model.Book;
import lt.sda.academy.printers.AuthorPrinter;
import lt.sda.academy.printers.BookPrinter;
import lt.sda.academy.util.ScannerUtil;

import java.util.List;
import java.util.Optional;

import static lt.sda.academy.Main.getAuthorServiceInstance;

public class BookService {
    private final BookRepository REPOSITORY;

    public BookService(BookRepository bookRepository) {
        this.REPOSITORY = bookRepository;
    }

    public void findAllBooks() {
        int limit = 10;
        int offset = 0;
        int answer;
        do {
            List<Book> books = REPOSITORY.findAll(limit, offset);
            if (!BookPrinter.printBooks(books)) {
                return;
            }
            if (books.size() < 10) {
                System.out.println("Knygų sąrašo pabaiga.");
                return;
            }

            offset += limit;
            limit += 10;
            System.out.println("1: Matyti daugiau knygų");
            System.out.println("0: Išeiti");
            answer = ScannerUtil.SCANNER.nextInt();
        } while (answer == 1);
    }

    public void findByNameOrByAuthor() {
        int selection = BookPrinter.selectSearch();
        if (selection == 1) {
            String name = BookPrinter.getBookInformation("Įveskite knygos pavadinimą: ");
            List<Book> bookByName = REPOSITORY.findBookByName(name);
            printIfFoundByName(name, bookByName);
        } else if (selection == 2) {
            Long id = Long.valueOf(BookPrinter.getBookInformation("Įveskite autoriaus id: "));
            List<Book> booksByAuthorId = REPOSITORY.findBookByAuthorId(id);
            printIfFoundByAuthorId(id, booksByAuthorId);
        } else {
            System.out.println("Neteisingas pasirinkimas");
        }
    }

    private void printIfFoundByAuthorId(Long id, List<Book> books) {
        if (!books.isEmpty()) {
            System.out.printf("Rastos knygos su autoriaus id %d:\n", id);
            System.out.println(books);
        } else {
            System.out.printf("Knygų su autoriaus id %d nerasta.\n", id);
        }
    }

    private void printIfFoundByName(String name, List<Book> books) {
        if (!books.isEmpty()) {
            System.out.printf("Rastos knygos su pavadinimu %s:\n", name);
            System.out.println(books);
        } else {
            System.out.printf("Knygų su pavadinimu %s nerasta.\n", name);
        }
    }


    public void createBook() {
        String name = BookPrinter.getBookInformation("Knygos pavadinimas: ");
        String description = BookPrinter.getBookInformation("Knygos aprašymas: ");
        Long id = AuthorPrinter.askAuthorID();
        Author author = getAuthorServiceInstance().findAuthorById(id);
        REPOSITORY.save(new Book(name, description, author));
    }

    public void updateBook() {
        REPOSITORY.updateBook();
    }

    public void deleteBook() {
        REPOSITORY.deleteBook();
    }

    public Optional<Book> find(Long bookId) {
        return REPOSITORY.find(bookId);
    }

    public void addBook(Book book) {
        REPOSITORY.save(book);
    }
}
