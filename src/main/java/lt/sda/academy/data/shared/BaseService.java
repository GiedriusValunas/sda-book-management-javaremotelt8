package lt.sda.academy.data.shared;

import lt.sda.academy.data.repositories.shared.BaseRepository;
import lt.sda.academy.model.shared.BaseEntity;

import java.util.Optional;

public class BaseService <ID, T extends BaseEntity, R extends BaseRepository<ID, T>> {
    public final R REPOSITORY;

    public BaseService(R repository) {
        this.REPOSITORY = repository;
    }

    public Optional<T> find(ID id) {
        return REPOSITORY.find(id);
    }
}