package lt.sda.academy.data.repositories;

import lt.sda.academy.data.repositories.shared.BaseRepository;
import lt.sda.academy.model.Author;
import lt.sda.academy.model.Book;
import lt.sda.academy.printers.AuthorPrinter;
import lt.sda.academy.printers.BookPrinter;
import lt.sda.academy.util.ScannerUtil;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import java.util.List;

import static lt.sda.academy.Main.getAuthorServiceInstance;

public class BookRepository extends BaseRepository<Long, Book> {
    public BookRepository(EntityManager entityManager, Class<Book> entityClass) {
        super(entityManager, entityClass);
    }

    public List<Book> findBookByName(String name) {
        return entityManager.createQuery("SELECT b " +
                        "FROM " + Book.class.getSimpleName() + " b " +
                        "WHERE b.name LIKE :bookName", Book.class)
                .setParameter("bookName", name)
                .getResultList();
    }

    public List<Book> findBookByAuthorId(Long authorId) {
        return entityManager.createQuery("SELECT b " +
                        "FROM " + Book.class.getSimpleName() + " b " +
                        "WHERE b.author.id = :id", Book.class)
                .setParameter("id", authorId)
                .getResultList();
    }

    public void updateBook() {
        Book book = findBookById(BookPrinter.askBookId());
        System.out.println("Knyga rasta:\n" + book);
        System.out.println("Pakeisti pavadinimą? Spausk 1");
        if (ScannerUtil.SCANNER.nextInt() == 1) {
            String newName = BookPrinter.getBookInformation("Įrašyk naują pavadinimą");
            book.setName(newName);
        }
        System.out.println("Pakeisti aprašymą? Spausk 1");
        if (ScannerUtil.SCANNER.nextInt() == 1) {
            String newDescription = BookPrinter.getBookInformation("Įrašyk naują aprašymą: ");
            book.setDescription(newDescription);
        }
        System.out.println("Pakeisti autorių? Spausk 1");
        if (ScannerUtil.SCANNER.nextInt() == 1) {
            Long id = AuthorPrinter.askAuthorID();
            Author newAuthor = getAuthorServiceInstance().findAuthorById(id);
            book.setAuthor(newAuthor);
        }
        save(book);
        System.out.println("Knyga pakeista į : " + book);
    }

    public Book findBookById(Long bookId) {
        return find(bookId).orElseThrow(
                () -> new EntityNotFoundException("Knyga su ID " + bookId + " neegzistuoja")
        );
    }

    public void deleteBook() {
        Long bookId = BookPrinter.askBookId();
        Book book = findBookById(bookId);
        delete(book);
        System.out.println("Ištrinta " + book);
    }
}
