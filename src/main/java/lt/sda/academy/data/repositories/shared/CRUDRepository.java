package lt.sda.academy.data.repositories.shared;

import lt.sda.academy.model.shared.BaseEntity;

import java.util.List;
import java.util.Optional;

public interface CRUDRepository<ID, T extends BaseEntity> {
    // Create/Update
    boolean save(T entity);

    // Read
    Optional<T> find(ID id);

    List<T> findAll();
    List<T> findAll(int limit, int offset);

    // Delete
    void delete(T entity);
}
