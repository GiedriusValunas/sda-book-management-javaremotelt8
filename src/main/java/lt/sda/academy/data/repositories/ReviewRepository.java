package lt.sda.academy.data.repositories;

import lt.sda.academy.data.repositories.shared.BaseRepository;
import lt.sda.academy.model.Review;

import javax.persistence.EntityManager;
import java.util.List;

public class ReviewRepository extends BaseRepository<Long, Review> {
    public ReviewRepository(EntityManager entityManager, Class<Review> entityClass) {
        super(entityManager, entityClass);
    }
    public List<Review> findReviewsByBookId(Long bookId) {
        return entityManager.createQuery("FROM " + Review.class.getSimpleName() + " r " +
                        "WHERE r.book.id = ?1", Review.class)
                .setParameter(1, bookId)
                .getResultList();
    }
}
