package lt.sda.academy.data.repositories;

import lt.sda.academy.data.repositories.shared.BaseRepository;
import lt.sda.academy.model.Author;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import java.util.List;

public class AuthorRepository extends BaseRepository<Long, Author> {

    public AuthorRepository(EntityManager entityManager, Class<Author> entityClass) {
        super(entityManager, entityClass);
    }

    public List<Author> findByKeyword(String word) {
        return entityManager.createQuery("SELECT a " +
                                "FROM " + Author.class.getSimpleName() + " a " +
                                "WHERE a.firstname LIKE ?1 OR a.lastname LIKE ?1",
                        Author.class)
                .setParameter(1, word)
                .getResultList();
    }

    public Author findAuthorById(Long authorId) {
        return find(authorId).orElseThrow(
                () -> new EntityNotFoundException("Autorius su ID " + authorId + " neegzistuoja")
        );
    }
}
