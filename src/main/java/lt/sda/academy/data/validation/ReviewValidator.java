package lt.sda.academy.data.validation;

import lt.sda.academy.model.Review;
import lt.sda.academy.printers.ReviewPrinter;

public class ReviewValidator {

    public static boolean validateReview(Review review) {
        if (review.getBook().getName() == null) {
            ReviewPrinter.printBookByIDNotFound(review.getBook().getId());
            return false;
        }
        if (review.getRating() < 0 || review.getRating() > 10) {
            ReviewPrinter.printBookRatingOutOfBounds(review.getRating());
            return false;
        }
        return true;
    }
}
