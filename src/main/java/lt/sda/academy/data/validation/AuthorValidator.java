package lt.sda.academy.data.validation;

import lt.sda.academy.model.Author;
import lt.sda.academy.printers.AuthorPrinter;

public class AuthorValidator {
    public static boolean validateAuthor(Author author) {
        String regex = ".*\\d.*";
        if (isNullOrEmpty(author.getFirstname()) || author.getFirstname().matches(regex)) {
            AuthorPrinter.printAuthorValidationError(author.getFirstname());
            return false;
        }
        if (isNullOrEmpty(author.getLastname()) || author.getLastname().matches(regex)) {
            AuthorPrinter.printAuthorValidationError(author.getLastname());
            return false;
        }
        return true;
    }

    private static boolean isNullOrEmpty(String string) {
        return string == null || string.isEmpty();
    }
}
