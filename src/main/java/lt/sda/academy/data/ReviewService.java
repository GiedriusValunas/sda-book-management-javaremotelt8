package lt.sda.academy.data;

import lt.sda.academy.Main;
import lt.sda.academy.data.repositories.ReviewRepository;
import lt.sda.academy.model.Book;
import lt.sda.academy.model.Review;
import lt.sda.academy.printers.BookPrinter;
import lt.sda.academy.printers.ReviewPrinter;

import java.util.List;

public class ReviewService {
    private final ReviewRepository REPOSITORY;

    public ReviewService(ReviewRepository repository) {
        this.REPOSITORY = repository;
    }

    public void showReviews() {
        int limit = 10, offset = 0;
        List<Review> reviews;
        do {
            reviews = REPOSITORY.findAll(limit, offset);
            if (!reviews.isEmpty()) {
                ReviewPrinter.printReviews(reviews);
                offset += limit;
                if (reviews.size() == limit && !ReviewPrinter.continuePrintReviews())
                    break;
            }
        } while (reviews.size() == limit);
        if (!reviews.isEmpty()) {
            ReviewPrinter.printReviewListEnd();
        } else ReviewPrinter.printReviewListIsEmpty();
    }

    public void showBookReviews() {
        List<Review> reviews;
        Long bookId = BookPrinter.askBookId();
        Book book = Main.getBookServiceInstance().find(bookId).orElse(null);
        if (book != null) {
            reviews = REPOSITORY.findReviewsByBookId(bookId);
            if (!reviews.isEmpty()) {
                ReviewPrinter.printReviews(reviews);
                ReviewPrinter.printReviewListEnd();
            } else ReviewPrinter.printBookHasNoReviews(book.getName());
        } else BookPrinter.printBookNotFound(bookId);
    }

    public void createReview() {
        Review review = ReviewPrinter.askReviewInfo();
        boolean created = review != null && REPOSITORY.save(review);
        ReviewPrinter.printReviewCreationMessage(created ? review : null);
    }

    public void addReview(Review review) {
        REPOSITORY.save(review);
    }
}
