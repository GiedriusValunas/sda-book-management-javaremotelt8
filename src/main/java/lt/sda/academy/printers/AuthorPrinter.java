package lt.sda.academy.printers;

import lt.sda.academy.data.validation.AuthorValidator;
import lt.sda.academy.model.Author;
import lt.sda.academy.util.ScannerUtil;

import java.util.List;

public class AuthorPrinter {

    public static void printAuthorListIsEmpty() {
        System.out.println("Autorių sąrašas tuščias.");
    }

    public static void printAuthorMenuChoices() {
        System.out.println("1. Matyti visus autorius.");
        System.out.println("2. Ieškoti autorių pagal vardą arba pavardę.");
        System.out.println("3. Pridėti autorių.");
        System.out.println("4. Atnaujinti autorių.");
        System.out.println("5. Ištrinti autorių pagal id.");
        System.out.println("0. Grįžti atgal.");
    }

    public static Author askAuthorInfo(Author author) {
        String firstName, lastName;
        do {
            System.out.println("Įveskite autoriaus vardą: ");
            ScannerUtil.SCANNER.nextLine();
            firstName = ScannerUtil.SCANNER.nextLine();
            if (firstName.equals(String.valueOf(0)))
                return null;

            System.out.println("Įveskite autoriaus pavardę: ");
            lastName = ScannerUtil.SCANNER.nextLine();
            author.setFirstname(firstName);
            author.setLastname(lastName);
        } while (!AuthorValidator.validateAuthor(author));
        return author;
    }

    public static void printAuthorValidationError(String text) {
        System.out.printf("\"%s\" netinkamas! Įveskite duomenis dar kartą arba atšaukite informaciją įvesdami 0.\n", text);
    }

    public static void printAuthorCreationMessage(Author author) {
        if (author == null) {
            System.out.println("Autoriaus nepavyko sukurti.");
        } else {
            System.out.printf("Buvo sukurtas %s %s, kurio id {%d}\n", author.getFirstname(), author.getLastname(), author.getId());
        }
    }

    public static void printAuthorUpdateMessage(Author author) {
        if (author == null) {
            System.out.println("Autoriaus nepavyko atnaujinti.");
        } else {
            System.out.println(author + "\n");
        }
    }

    public static void printAuthors(List<Author> authors) {
        for (Author author : authors) {
            System.out.println(author);
        }
    }

    public static boolean continuePrintAuthors() {
        System.out.println("Spausdinti sekančius autorius? 1 - taip, 0 - ne.");
        boolean print = false, repeat = true;
        while (repeat) {
            try {
                print = ScannerUtil.SCANNER.nextBoolean();
                repeat = false;
            } catch (Exception e) {
                System.out.println("Neteisinga įvestis, bandykite dar kartą.");
            }
        }
        return print;
    }

    public static void printAuthorListEnd() {
        System.out.println("Autorių sąrašo pabaiga.\n");
    }

    public static String askAuthorKeyword() {
        System.out.println("Įveskite ieškomo autoriaus vardą arba pavardę (0 - atšaukti):");
        return ScannerUtil.SCANNER.next();
    }

    public static void printAuthorsNotFound(String word) {
        System.out.printf("Autorius nerastas su vardu \"%1$s\" ar pavarde \"%1$s\"\n", word);
    }

    public static Long askAuthorID() {
        long authorId;
        System.out.println("Įveskite pasirinkto autoriaus ID:");
        authorId = ScannerUtil.SCANNER.nextInt();
        return authorId;
    }

    public static void printAuthorByIDNotFound(long authorId) {
        System.out.printf("Autorius pagal id: {%d} nebuvo rastas.\n", authorId);
    }

    public static void printAuthorDeleteMessage(Author author) {
        System.out.println("Autorius " + author + " buvo sėkmingai ištrintas.");
    }
}
