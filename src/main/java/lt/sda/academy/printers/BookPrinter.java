package lt.sda.academy.printers;

import lt.sda.academy.data.BookService;
import lt.sda.academy.model.Book;
import lt.sda.academy.presentation.BookChoice;
import lt.sda.academy.util.ScannerUtil;

import java.util.List;

public class BookPrinter {

    public static void selectBookChoice(BookService bookService) {

        System.out.println("1.Matyti visas knygas");
        System.out.println("2.Ieškoti knygų pagal pavadinimą arba autorių");
        System.out.println("3.Pridėti knygą");
        System.out.println("4.Atnaujinti knygą");
        System.out.println("5.Ištrinti knygą pagal id");

        int selection = ScannerUtil.SCANNER.nextInt();
        BookChoice.performOperation(selection, bookService);

    }

    public static int selectSearch() {
        System.out.println("1.Ieškoti pagal knygos pavadinimą");
        System.out.println("2.Ieškoti pagal autorių");
        return ScannerUtil.SCANNER.nextInt();
    }

    public static boolean printBooks(List<Book> books) {
        if (books.isEmpty()) {
            System.out.println("Knygų sąrašas tuščias");
            return false;
        }
        for (Book book : books) {
            System.out.println(book);
        }
        return true;
    }

    public static String getBookInformation(String s) {
        System.out.println(s);
        ScannerUtil.SCANNER.nextLine();
        return ScannerUtil.SCANNER.nextLine();
    }

    public static Long askBookId() {
        System.out.println("Įveskite knygos ID: ");
        return ScannerUtil.SCANNER.nextLong();
    }

    public static void printBookNotFound(Long bookId) {
        System.out.printf("Knyga su ID %d nerasta.\n", bookId);
    }
}
