package lt.sda.academy.printers;

import lt.sda.academy.data.validation.ReviewValidator;
import lt.sda.academy.model.Book;
import lt.sda.academy.model.Review;
import lt.sda.academy.util.ScannerUtil;

import java.util.List;

import static lt.sda.academy.Main.getBookServiceInstance;

public class ReviewPrinter {

    public static void printReviewMenuChoices() {
        System.out.println("1.Matyti visus knygų ivertinimus.");
        System.out.println("2.Matyti įvertinimus knygai pagal id.");
        System.out.println("3.Pridėti naują įvertinimą knygai.");
        System.out.println("0.Grįžti atgal.");
    }

    public static Review askReviewInfo() {
        Review review = new Review();
        String comment;
        int rating;
        do {
            System.out.println("Įveskite knygos ID: ");
            long bookId = ScannerUtil.SCANNER.nextInt();
            if (bookId == 0)
                return null;
            Book book = getBookServiceInstance().find(bookId).orElse(new Book());
            if (book.getName() == null) book.setId(bookId);
            review.setBook(book);

            System.out.println("Įveskite įvertinimą: ");
            rating = ScannerUtil.SCANNER.nextInt();
            review.setRating(rating);

            System.out.println("Įveskite komentarą: ");
            ScannerUtil.SCANNER.nextLine();
            comment = ScannerUtil.SCANNER.nextLine();
            review.setComment(comment);
        } while (!ReviewValidator.validateReview(review));
        return review;
    }

    public static void printBookByIDNotFound(long id) {
        System.out.printf("Knyga su id: {%d} nebuvo rasta.\n", id);
    }

    public static void printBookRatingOutOfBounds(int rating) {
        System.out.printf("Atsiliepimo įvertinimas {%d} neteisingas, turi būti tarp 0 ir 10.\n", rating);
    }

    public static void printReviewCreationMessage(Review review) {
        if (review == null) {
            System.out.println("Atsiliepimo nepavyko sukurti.");
        } else {
            System.out.println("Buvo sukurtas atsiliepimas: " + review);
        }
    }

    public static void printReviews(List<Review> reviews) {
        for (Review review : reviews) {
            System.out.println(review);
        }
    }

    public static boolean continuePrintReviews() {
        System.out.println("Spausdinti sekančius atsiliepimus? 1 - taip, 0 - ne.");
        boolean print = false, repeat = true;
        while (repeat) {
            try {
                print = ScannerUtil.SCANNER.nextBoolean();
                repeat = false;
            } catch (Exception e) {
                System.out.println("Neteisinga įvestis, bandykite dar kartą.");
            }
        }
        return print;
    }

    public static void printReviewListEnd() {
        System.out.println("Atsiliepimų sąrašo pabaiga.");
    }

    public static void printBookHasNoReviews(String bookName) {
        System.out.printf("Knyga \"%s\" neturi atsiliepimų.\n", bookName);
    }

    public static void printReviewListIsEmpty() {
        System.out.println("Nėra atsiliepimų apie knygas.");
    }
}
