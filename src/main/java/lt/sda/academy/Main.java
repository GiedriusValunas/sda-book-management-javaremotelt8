package lt.sda.academy;

import lt.sda.academy.data.AuthorService;
import lt.sda.academy.data.BookService;
import lt.sda.academy.data.ReviewService;
import lt.sda.academy.data.repositories.AuthorRepository;
import lt.sda.academy.data.repositories.BookRepository;
import lt.sda.academy.data.repositories.ReviewRepository;
import lt.sda.academy.model.Author;
import lt.sda.academy.model.Book;
import lt.sda.academy.model.Review;
import lt.sda.academy.presentation.ApplicationGreeting;
import lt.sda.academy.presentation.AuthorChoice;
import lt.sda.academy.presentation.ReviewChoice;
import lt.sda.academy.printers.BookPrinter;
import lt.sda.academy.util.HibernateUtil;

import javax.persistence.EntityManager;

public class Main {
    private static final EntityManager ENTITY_MANAGER = HibernateUtil.getSessionFactory().createEntityManager();
    private static final AuthorService AUTHOR_SERVICE = new AuthorService(new AuthorRepository(ENTITY_MANAGER, Author.class));
    private static final BookService BOOK_SERVICE = new BookService(new BookRepository(ENTITY_MANAGER, Book.class));
    private static final ReviewService REVIEW_SERVICE = new ReviewService(new ReviewRepository(ENTITY_MANAGER, Review.class));

    public static void main(String[] args) {
        initData();
        int choice = 0;
        while (choice != 4) {

            choice = ApplicationGreeting.selectMainMenuWindow();
            switch (choice) {
                case 1:
                    AuthorChoice.selectAuthorsChoices(AUTHOR_SERVICE);
                    break;
                case 2:
                    BookPrinter.selectBookChoice(BOOK_SERVICE);
                    break;
                case 3:
                    ReviewChoice.selectReviewChoice(REVIEW_SERVICE);
            }
        }
        ENTITY_MANAGER.close();
    }

    public static AuthorService getAuthorServiceInstance() {
        return AUTHOR_SERVICE;
    }

    public static BookService getBookServiceInstance() {
        return BOOK_SERVICE;
    }

    public static void initData() {
        Author author1 = new Author();
        author1.setFirstname("Vardenis");
        author1.setLastname("Pavardenis");
        Author author2 = new Author();
        author2.setFirstname("Jonas");
        author2.setLastname("Jonaitis");
        AUTHOR_SERVICE.addAuthor(author1);
        AUTHOR_SERVICE.addAuthor(author2);
        Book book1 = new Book();
        book1.setAuthor(author1);
        book1.setName("Pirmoji knyga");
        book1.setDescription("Pavardenio pirmoji knyga.");
        Book book2 = new Book();
        book2.setAuthor(author2);
        book2.setName("Antroji knyga");
        book2.setDescription("Jonaičio pirmoji knyga.");
        BOOK_SERVICE.addBook(book1);
        BOOK_SERVICE.addBook(book2);
        Review review1 = new Review();
        review1.setBook(book1);
        review1.setRating(10);
        review1.setComment("Puiki knyga.");
        Review review2 = new Review();
        review2.setBook(book2);
        review2.setRating(10);
        review2.setComment("Skaityčiau ir sakaityčiau.");
        REVIEW_SERVICE.addReview(review1);
        REVIEW_SERVICE.addReview(review2);
    }
}
